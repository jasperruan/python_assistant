#test
from urllib import request
from icrawler.builtin import GoogleImageCrawler
from bs4 import BeautifulSoup as soup
import webbrowser
from weather import Weather, Unit
import pyttsx3
from gtts import gTTS
import os
import time


def print_website_code(url):
    response = request.urlopen(url)
    html = response.read().decode('utf-8')
    print(html)
    return html

def get_google_image(keyword):
    google_crawler = GoogleImageCrawler(storage={'root_dir':'Google Image Result-'+keyword})
    google_crawler.crawl(keyword=keyword, max_num=10)
    print('done')

def get_data_from_pb(url, keyword):
    uClient = request.urlopen(url)
    content = uClient.read()
    uClient.close()
    page_soup = soup(content, 'html.parser')
    item_soup = page_soup.findAll('div',{'class':'item'})
    item_name = []
    price = []
    for item in item_soup:
        item_description = item.div.div.a.span
        item_name.append(item_description.text)
    price_tag = page_soup.findAll('div', {'class':'item_buy'})
    price_exgst = []
    for price in price_tag:
        full_price_tag = price.span.span.findAll('span', {'class':'price_full'})[0].text
        price_exgst.append(get_after_gst(full_price_tag))
    i = 0
    file_name = 'monitor_price.txt'
    file_out = open(file_name, 'w')
    while i < len(item_name):
        print(item_name[i])
        print(price_exgst[i])
        file_out.write(item_name[i] + '\n')
        file_out.write(str(price_exgst[i]) + '\n')
        i+=1
    file_out.close()
    webbrowser.open(file_name)


def get_after_gst(number_text):
    number_list = [0,1,2,3,4,5,6,7,8,9,'0','1','2','3','4','5','6','7','8','9', '.']
    while number_text[0] not in number_list:
        number_text = number_text[1:]
    digit_text = ''
    for n in number_text:
        if n in number_list:
            digit_text += n
    return round(float(digit_text)*1.15,2)
def write_file(data, file_name):
    file_out = open(file_name, 'w')
    file_out.write(data)
    file_out.close()

def get_now_showing_movies():
    uClient = request.urlopen('https://www.flicks.co.nz/now-playing/?sortby=flicks_rating')
    content = uClient.read()
    uClient.close()
    page_soup = soup(content, 'html.parser')
    item_soup = page_soup.findAll('ul', {'class':'block-list block-list--collapse article-list'})[0].findAll('li')
    now_showing_name = []
    for item in item_soup:
        if item.div.div.div != None:
            item_tag = item.div.div.div.findAll('div', {'class': 'grid__item three-quarters article-item__content'})[0].div.h3.a.text.strip()
            now_showing_name.append(item_tag)
            print(item_tag)
    return now_showing_name

def get_weather(location):
    weather = Weather(unit=Unit.CELSIUS)
    location = weather.lookup_by_location(location)
    condition = location.condition
    #print(condition.low)

    forecasts = location.forecast
    for forecast in forecasts:
        weather_info = forecast.date + ': ' + forecast.text + '  ' + forecast.high + ' - ' + forecast.low
        print(weather_info)

def text_to_speech(text):
    engine = pyttsx3.init()
    engine.setProperty('rate', 150)
    engine.setProperty('voice', 'HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Speech\Voices\Tokens\TTS_MS_EN-US_ZIRA_11.0')

    for voice in engine.getProperty('voices'):
        print(voice)
        engine.setProperty('voice', voice.id)
        engine.say(text)
        engine.runAndWait()
def text_to_speech_v2(text):
    tts = gTTS(text=text, lang='en')
    tts.save("good.mp3")
    os.system("mpg321 good.mp3")

def get_movie_download_avaliable():
    uClient = request.urlopen('https://www.dytt8.net/index.htm')
    content = uClient.read()
    uClient.close()
    page_soup = soup(content, 'html.parser', fromEncoding="GB18030")
    section_soup = page_soup.findAll('div', {'class': 'bd3r'})[0].div.div.findAll('div', {'class':'co_area2'})
    new_movie_section = section_soup[0].findAll('div', {'class':'co_content8'})[0].ul.findAll('tr')
    movie_resource_section = section_soup[1].findAll('div', {'class':'co_content8'})[0].ul.findAll('tr')
    tv_show_section = section_soup[4].findAll('div', {'class':'co_content3'})[0].ul.findAll('tr')

    new_movie_dict = {}
    for new_movie in new_movie_section:
        if len(new_movie.td.findAll('a')) > 1:
            movie_name = new_movie.td.findAll('a')[1].text
            index_1 = str(new_movie.td.findAll('a')[1]).find('"')
            index_2 = str(new_movie.td.findAll('a')[1]).rfind('"')
            movie_link = 'https://www.dytt8.net' + str(new_movie.td.findAll('a')[1])[index_1+1:index_2]
            new_movie_dict[movie_name] = movie_link
    movie_resource_dict = {}
    for movie_resource in movie_resource_section:
        if len(movie_resource.td.findAll('a')) > 1:
            movie_name = movie_resource.td.findAll('a')[1].text
            index_1 = str(movie_resource.td.findAll('a')[1]).find('"')
            index_2 = str(movie_resource.td.findAll('a')[1]).rfind('"')
            movie_link = 'https://www.dytt8.net' + str(movie_resource.td.findAll('a')[1])[index_1 + 1:index_2]
            movie_resource_dict[movie_name] = movie_link
    tv_show_dict = {}
    for tv_show in tv_show_section:
        if len(movie_resource.td.findAll('a')) > 1:
            movie_name = tv_show.td.findAll('a')[1].text
            index_1 = str(tv_show.td.findAll('a')[1]).find('"')
            index_2 = str(tv_show.td.findAll('a')[1]).rfind('"')
            movie_link = 'https://www.dytt8.net' + str(tv_show.td.findAll('a')[1])[index_1 + 1:index_2]
            tv_show_dict[movie_name] = movie_link
    for key, value in new_movie_dict.items():
        print(key + ' : ' + value)
    for key, value in movie_resource_dict.items():
        print(key + ' : ' + value)
    for key, value in tv_show_dict.items():
        print(key + ' : ' + value)

def access_website_different_ip():
    a = 1
    sleep_time = 6
    while a < 20:
        webbrowser.open('http://crosee.iprevert.com/r.php?nin_u=Oi8vcGF5ZW56LmNvLm56&nin_b=1&nin_f=norefer')
        time.sleep(sleep_time)
        webbrowser.open('http://108.61.45.50/~iprevert/r.php?nin_u=Oi8vcGF5ZW56LmNvLm56&nin_b=1&nin_f=norefer')
        time.sleep(sleep_time)
        webbrowser.open('http://joyeed.iprevert.com/r.php?nin_u=Oi8vcGF5ZW56LmNvLm56&nin_b=1&nin_f=norefer')
        time.sleep(sleep_time)
        webbrowser.open('http://108.61.45.50/~iprevert/r.php?nin_u=Oi8vcGF5ZW56LmNvLm56&nin_b=1&nin_f=norefer')
        time.sleep(sleep_time)
        webbrowser.open('http://192.99.32.191/~iprevert/r.php?nin_u=Oi8vcGF5ZW56LmNvLm56&nin_b=1&nin_f=norefer')
        time.sleep(sleep_time)
        webbrowser.open('http://107.173.162.245/~opuvrvxf/r.php?nin_u=Oi8vcGF5ZW56LmNvLm56&nin_b=1&nin_f=norefer')
        time.sleep(sleep_time)
        webbrowser.open('http://gielw.iprevert.com/r.php?nin_u=Oi8vcGF5ZW56LmNvLm56&nin_b=1&nin_f=norefer')
        time.sleep(sleep_time)
        webbrowser.open('http://107.161.23.124/~iprevert/r.php?nin_u=Oi8vcGF5ZW56LmNvLm56&nin_b=1&nin_f=norefer')
        time.sleep(sleep_time)
        webbrowser.open('http://grewod.iprevert.com/r.php?nin_u=Oi8vcGF5ZW56LmNvLm56&nin_b=1&nin_f=norefer')
        time.sleep(sleep_time)
        webbrowser.open('http://jusmpp.iprevert.com/r.php?nin_u=Oi8vcGF5ZW56LmNvLm56&nin_b=1&nin_f=norefer')
        time.sleep(sleep_time)
        webbrowser.open('http://167.88.160.226/~iprevert/r.php?nin_u=Oi8vcGF5ZW56LmNvLm56&nin_b=1&nin_f=norefer')
        time.sleep(sleep_time)
        webbrowser.open('http://23.95.4.194/~iprevert/r.php?nin_u=Oi8vcGF5ZW56LmNvLm56&nin_b=1&nin_f=norefer')
        time.sleep(sleep_time)
        webbrowser.open('http://aeso.iprevert.com/r.php?nin_u=Oi8vcGF5ZW56LmNvLm56&nin_b=1&nin_f=norefer')
        time.sleep(sleep_time)
        webbrowser.open('http://aerre.iprevamp.com/r.php?nin_u=Oi8vcGF5ZW56LmNvLm56&nin_b=1&nin_f=norefer')
        a += 1
def sky_kiwi_job():
    uClient = request.urlopen('http://bbs.skykiwi.com/forum.php?mod=forumdisplay&fid=55')
    content = uClient.read()
    uClient.close()
    page_soup = soup(content, 'html.parser', fromEncoding="GB18030")
    section_soup = page_soup.findAll('div', {'id':'threadlist'})[0].findAll('div', {'class':'bm_c'})[0].form.table.findAll('th', {'class':'new'})
    job_dict = {}
    for title_tag in section_soup:
        title = title_tag.findAll('a', {'class':'xst'})[0]
        name = title.text
        index_1 = str(title).find('href=')
        string_1 = str(title)[index_1:]
        index_1 = string_1.find('"')
        string_1 = string_1[index_1+1 : ]
        index_1 = string_1.find('"')
        string_1 = string_1[:index_1]
        index_1 = string_1.find('amp;')
        string_2 = string_1[:index_1]
        index_1 = string_1.find('tid')
        string_2 = string_2 + string_1[index_1:]
        job_dict[name] = 'http://bbs.skykiwi.com/'+string_2
    for x, y in job_dict.items():
        print(x, ':', y)
