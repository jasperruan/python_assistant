<?php
$default_income = 10000;
require_once "backend.php";
$income = isset($_POST['user_income'])? intval(htmlspecialchars($_POST['user_income'])): $default_income;
$frequency = isset($_POST['per_select'])? htmlspecialchars($_POST['per_select']):'Year';
$kiwisaver_select = isset($_POST['kiwisaver_select'])? htmlspecialchars($_POST['kiwisaver_select']):'None';
$studentloan_checkbox = isset($_POST['studentloan_checkbox'])? 'TRUE':'FALSE';
$employer_employee_radio = isset($_POST['radio'])? $_POST['radio']:'employer';
$income_year_show = get_income_year($income, $frequency);
$paye_year_show = get_paye_year($income_year_show);
$kiwisaver_year_show = get_kiwisaver_year($income_year_show, $kiwisaver_select);
$studentloan_year_show = get_studentloan_year($income_year_show, $studentloan_checkbox);
$acc_year_show = get_acc_year($income_year_show);
$total_tax_year_show = $acc_year_show + $kiwisaver_year_show + $studentloan_year_show + $paye_year_show;
$net_pay_year_show = $income_year_show - $total_tax_year_show;
$average_tax_rate_show = get_average_tax_rate($total_tax_year_show, $income_year_show);

$income_month_show = $income_year_show/12;
$paye_month_show = $paye_year_show/12;
$kiwisaver_month_show = $kiwisaver_year_show/12;
$studentloan_month_show = $studentloan_year_show/12;
$acc_month_show = $acc_year_show/12;
$total_tax_month_show = $total_tax_year_show/12;
$net_pay_month_show = $net_pay_year_show/12;

$income_biweek_show = $income_year_show/26;
$paye_biweek_show = $paye_year_show/26;
$kiwisaver_biweek_show = $kiwisaver_year_show/26;
$studentloan_biweek_show = $studentloan_year_show/26;
$acc_biweek_show = $acc_year_show/26;
$total_tax_biweek_show = $total_tax_year_show/26;
$net_pay_biweek_show = $net_pay_year_show/26;

$income_week_show = $income_year_show/52;
$paye_week_show = $paye_year_show/52;
$kiwisaver_week_show = $kiwisaver_year_show/52;
$studentloan_week_show = $studentloan_year_show/52;
$acc_week_show = $acc_year_show/52;
$total_tax_week_show = $total_tax_year_show/52;
$net_pay_week_show = $net_pay_year_show/52;
//HTML head variable
$keywords_content = "$10000 paye, $10000 income tax calculator, $10000 payroll calculator, $10000 payroll, $10000 paye nz, $10000 paye calculator nz, $10000 paye calculator, $10000 tax calculator, $10000 pay calculator, $10000 payroll tax calculator" ;
$description = "$10000 Calculate income tax with NZ's most popular PAYE calculators. Calculate PAYE, Student Loan, Kiwisave and Net Pay.";
$web_title = "$10,000 No.1, most popular PAYE Calculator in New Zealand";
?>

<?php include 'content.php' ?>